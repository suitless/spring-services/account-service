package nl.suitless.accountservice.Web.HateoasResources;

import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Web.Controllers.AccountController;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class AccountsResource extends ResourceSupport {
    private List<Account> accounts;

    public AccountsResource(List<Account> accounts) {
        this.accounts = accounts;
        add(linkTo(AccountController.class).withRel("account"));
    }

    public AccountsResource() {}

    public List<Account> getAccounts() {
        return accounts;
    }

}
