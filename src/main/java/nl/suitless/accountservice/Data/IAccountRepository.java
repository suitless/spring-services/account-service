package nl.suitless.accountservice.Data;

import nl.suitless.accountservice.Domain.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;
import java.util.List;
import java.util.Set;

public interface IAccountRepository extends CrudRepository<Account, String> {
    Optional<Account> findByEmail(String email);
    Optional<List<Account>> getAllByFirstNameIsNotNull();
}
