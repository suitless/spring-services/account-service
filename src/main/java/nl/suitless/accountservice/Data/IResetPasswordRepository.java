package nl.suitless.accountservice.Data;

import nl.suitless.accountservice.Domain.ResetPassword;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IResetPasswordRepository extends CrudRepository<ResetPassword, Integer> {
    Optional<ResetPassword> findByEmail(String email);
}
