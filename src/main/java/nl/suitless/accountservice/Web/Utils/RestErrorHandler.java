package nl.suitless.accountservice.Web.Utils;

import nl.suitless.accountservice.Exceptions.AccountAlreadyExistsException;
import nl.suitless.accountservice.Exceptions.AccountNotFoundException;
import nl.suitless.accountservice.Exceptions.ResetTokenNotMatchedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class RestErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = AccountNotFoundException.class)
    public final ResponseEntity<ErrorDetails> handleAccountNotFoundException (AccountNotFoundException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = AccountAlreadyExistsException.class)
    public final ResponseEntity<ErrorDetails> handleAccountAlreadyExistsException(AccountAlreadyExistsException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(value = ResetTokenNotMatchedException.class)
    public final ResponseEntity<ErrorDetails> handleResetTokenNotMatchedException(ResetTokenNotMatchedException e, WebRequest request) {
        ErrorDetails details = new ErrorDetails(new Date(), e.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }
}
