package nl.suitless.accountservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class ResetAccountRequestModel {
    @NotNull
    private String email;
    @NotNull
    private String resetToken;
    @NotNull
    private String newPassword;

    public ResetAccountRequestModel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
