package nl.suitless.accountservice.Data;

import nl.suitless.accountservice.Domain.Role;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface IRoleRepository extends CrudRepository<Role, Integer> {
    Optional<Role> findByName(String name);
}
