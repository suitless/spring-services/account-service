package nl.suitless.accountservice.Web.HateoasResources;

import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Web.Controllers.AccountController;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class AccountResource extends ResourceSupport {
    private Account account;

    public AccountResource(Account account) {
        this.account = account;
        add(linkTo(AccountController.class).withRel("account"));
    }

    public AccountResource() {}

    public Account getAccount() {
        return account;
    }

}
