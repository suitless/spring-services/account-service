package nl.suitless.accountservice.Exceptions;

public class ResetTokenNotMatchedException extends RuntimeException {
    public ResetTokenNotMatchedException(String exception) { super(exception); }
}
