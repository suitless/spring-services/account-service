package nl.suitless.accountservice.Exceptions;

public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(String exception) {
        super(exception);
    }
}
