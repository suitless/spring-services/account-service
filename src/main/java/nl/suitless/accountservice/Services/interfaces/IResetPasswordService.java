package nl.suitless.accountservice.Services.interfaces;

import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.ResetPassword;
import nl.suitless.accountservice.Exceptions.*;
import nl.suitless.accountservice.Exceptions.ResetTokenNotMatchedException;

public interface IResetPasswordService {
    /**
     * Create and reset password token and connects it with the account email
     * @param email the email of the user
     * @return the created <b>ResetPasswordToken</b> entity
     */
    ResetPassword createResetPasswordToken(String email, String resetPasswordUrl);
    /**
     * Create new password after verifying the reset token
     * @param account the account of the user
     * @param resetToken the reset token of the user
     * @param newPassword the new password
     * @throws ResetTokenNotMatchedException if the reset token doesn't match with the user account
     */
    void resetPassword(Account account, String resetToken, String newPassword);
}
