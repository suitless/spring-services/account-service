package nl.suitless.accountservice;

import nl.suitless.accountservice.Data.IAccountRepository;
import nl.suitless.accountservice.Data.IResetPasswordRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.ResetPassword;
import nl.suitless.accountservice.Domain.Role;
import nl.suitless.accountservice.Web.HateoasResources.AccountResource;
import nl.suitless.accountservice.Web.HateoasResources.AccountsResource;
import nl.suitless.accountservice.Web.Utils.ErrorDetails;
import nl.suitless.accountservice.Web.Wrapper.CreateAccountRequestModel;
import nl.suitless.accountservice.Web.Wrapper.ResetAccountRequestModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {AccountServiceApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations = "classpath:application-test.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class AccountControllerIntegrationTests {
    @Autowired
    private IAccountRepository accountRepository;
    @Autowired
    private IResetPasswordRepository resetPasswordRepository;
    @Autowired
    private TestRestTemplate template = new TestRestTemplate();

    private Account account;
    private ResetPassword resetPassword;

    @Before
    public void setUp() {
        account = new Account("mail@mail.nl", "password", "jan", "piet");
        Role role = new Role("USER");

        account.getRoles().add(role);
        role.getAccounts().add(account);

        resetPassword = new ResetPassword(account.getEmail(), "http://resetUrl/");
        resetPassword = resetPasswordRepository.save(resetPassword);

        account = accountRepository.save(account);
    }

    @Test
    public void getAccountByIDValid() {

        ResponseEntity<AccountResource> receivedAccount = template.getForEntity("/" + account.getAccountID(), AccountResource.class);
        Account foundAccount = Objects.requireNonNull(receivedAccount.getBody()).getAccount();

        Assert.assertNotNull(foundAccount);
        Assert.assertEquals(foundAccount.getEmail(), account.getEmail());
        Assert.assertEquals(HttpStatus.OK, receivedAccount.getStatusCode());
    }

    @Test
    public void getAccountByIDInvalid() {
        ResponseEntity<ErrorDetails> receivedError = template.getForEntity("/100", ErrorDetails.class);
        ErrorDetails errorDetails = receivedError.getBody();

        Assert.assertEquals(HttpStatus.NOT_FOUND, receivedError.getStatusCode());
        Assert.assertNotNull(errorDetails);
    }

    @Test
    public void getAllAccountsValid() {
        ResponseEntity<AccountsResource> getAllAccounts = template.getForEntity("/", AccountsResource.class);
        List<Account> foundAccounts = Objects.requireNonNull(getAllAccounts.getBody()).getAccounts();

        Assert.assertNotEquals(foundAccounts.size(), 0);
        Assert.assertEquals(HttpStatus.OK, getAllAccounts.getStatusCode());
    }

    @Test
    public void createAccountValid() {
        CreateAccountRequestModel reqModel = new CreateAccountRequestModel();
        reqModel.setEmail("bob@hotmail.com");
        reqModel.setPassword("password");
        reqModel.setFirstName("spoiler");
        reqModel.setLastName("do");

        ResponseEntity<AccountResource> createdAccount = template.postForEntity("/", reqModel, AccountResource.class);
        Account response = Objects.requireNonNull(createdAccount.getBody()).getAccount();

        Assert.assertEquals(HttpStatus.CREATED, createdAccount.getStatusCode());
        Assert.assertNotNull(response);
        Assert.assertEquals(reqModel.getEmail(), response.getEmail());
    }

    @Test
    public void createAccountAlreadyExists() {
        Account account = new Account("mail@mail.nl", "password", "jan", "piet");
        ResponseEntity<ErrorDetails> receivedAccount = template.postForEntity("/", account, ErrorDetails.class);
        ErrorDetails response = receivedAccount.getBody();

        Assert.assertEquals(HttpStatus.CONFLICT, receivedAccount.getStatusCode());
        Assert.assertNotNull(response);
    }

    @Test
    public void deleteAccountValid() {
        ResponseEntity<Void> receivedAccount = template.exchange("/" + account.getAccountID(), HttpMethod.DELETE, null, Void.class);

        Assert.assertEquals(HttpStatus.OK, receivedAccount.getStatusCode());
    }

    @Test
    public void deleteAccountDoesNotExist() {
        ResponseEntity<ErrorDetails> receivedAccount = template.exchange("/3", HttpMethod.DELETE, null, ErrorDetails.class);
        ErrorDetails response = receivedAccount.getBody();

        Assert.assertEquals(HttpStatus.NOT_FOUND, receivedAccount.getStatusCode());
        Assert.assertNotNull(response);
    }

    @Test
    public void resetPasswordValid() {
        ResetAccountRequestModel reqModel = new ResetAccountRequestModel();
        reqModel.setEmail(account.getEmail());
        reqModel.setNewPassword("NEW PASSWORD");
        reqModel.setResetToken(resetPassword.getResetToken());
        ResponseEntity<Void> receivedAccount = template.postForEntity("/reset", reqModel, Void.class);

        Assert.assertEquals(HttpStatus.OK, receivedAccount.getStatusCode());
    }

    @Test
    public void resetPasswordResetTokenNotMatchedEmailNotFound() {
        ResetAccountRequestModel reqModel = new ResetAccountRequestModel();
        reqModel.setEmail("random@nemail.com");
        reqModel.setNewPassword("NEW PASSWORD");
        reqModel.setResetToken(resetPassword.getResetToken());
        ResponseEntity<Void> receivedAccount = template.postForEntity("/reset", reqModel, Void.class);

        Assert.assertEquals(HttpStatus.NOT_FOUND, receivedAccount.getStatusCode());
    }

    @Test
    public void resetPasswordResetTokenNotMatchedTokenNotCorrect() {
        ResetAccountRequestModel reqModel = new ResetAccountRequestModel();
        reqModel.setEmail(account.getEmail());
        reqModel.setNewPassword("NEW PASSWORD");
        reqModel.setResetToken("a8a998b4-df4b-4cbf-83ef-46b9c562ecd2");
        ResponseEntity<Void> receivedAccount = template.postForEntity("/reset", reqModel, Void.class);

        Assert.assertEquals(HttpStatus.BAD_REQUEST, receivedAccount.getStatusCode());
    }
}
