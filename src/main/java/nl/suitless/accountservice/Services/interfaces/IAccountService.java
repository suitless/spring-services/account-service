package nl.suitless.accountservice.Services.interfaces;

import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Exceptions.AccountAlreadyExistsException;
import nl.suitless.accountservice.Exceptions.AccountNotFoundException;

import java.util.List;

public interface IAccountService {
    /**
     * Creates a new account in the database with the given input
     * @param email the chosen email to register with
     * @param password the password chosen by the user
     * @param firstName the first name entered by the user
     * @param lastName the last name entered by the user
     * @return the created <b>Account</b> entity
     * @throws AccountAlreadyExistsException if user already exists
     */
    Account createAccount(String email, String password, String firstName, String lastName);

    /**
     * Updates an already existing account with the given input
     * @param id the id of the account you want to update
     * @param email the email of the account
     * @param password the password chosen by the user
     * @param firstName the first name entered by the user
     * @param lastName the last name entered by the user
     * @return the updated <b>Account</b> entity
     * @throws AccountNotFoundException if the account could not be found
     */
    Account updateAccount(String id, String email, String password, String firstName, String lastName);

    /**
     * Get the account by email
     * @param email the email the user registered with
     * @return <b>Account</b> of the user
     */
    Account getAccountByEmail(String email);

    /**
     * Gets account based on id given
     * @param id the id of the user you want to find
     * @return <b>Account</B> of the user
     * @throws AccountNotFoundException if user with given <b>ID</b> is not found in our system
     */
    Account getAccount(String id);

    /**
     * Get all accounts (admin only)
     * @return List of <b>Account</b> classes
     * @throws AccountNotFoundException if no accounts are found
     */
    List<Account> getAllAccounts();

    /**
     * Deleted user in the database with the given ID
     * @param id the id for the account you wish to delete
     * @return nothing, only a <b>HTTP STATUS CODE</b> will be returned to signal success/failure.
     */
    void deleteAccount(String id);
}
