package nl.suitless.accountservice.Exceptions;

public class AccountAlreadyExistsException extends RuntimeException {

    public AccountAlreadyExistsException(String exception) {
        super(exception);
    }
}
