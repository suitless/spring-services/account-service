# S45Proftaak

Dit is de back-end voor een applicatie in opdracht met LINC.

De back-end wordt gemaakt in Java (Gradle en Springboot)

### Inleiding van het concept:
Op dit moment zijn er veel studenten die een eigen onderneming beginnen, deze startups lopen helaas vaak tegen wettelijke problemen. Om dit te voorkomen kunnen startups ervoor kiezen om een juridisch persoon om advies te vragen, maar financieel gezien is dit niet altijd mogelijk voor kleine ondernemingen.

Om dit probleem op te lossen heeft ehv-LINC een plan bedacht om alle startups de mogelijkheid te geven om advies te verkrijgen. De startups kunnen op de applicatie aangeven tegen welke problemen ze aanlopen en krijgen vervolgens een resultaat met een passende oplossing. In de applicatie kunnen de gebruikers hun rapporten bekijken en nieuwere formulieren invullen. Als er juridische aanpassingen zijn zal de gebruiker een notificatie krijgen.