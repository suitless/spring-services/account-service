package nl.suitless.accountservice.Web.Wrapper;

import javax.validation.constraints.NotNull;

public class ForgotAccountRequestModel {
    @NotNull
    private String email;
    @NotNull
    private String resetPasswordUrl;

    public ForgotAccountRequestModel() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResetPasswordUrl() {
        return resetPasswordUrl;
    }

    public void setResetPasswordUrl(String resetPasswordUrl) {
        this.resetPasswordUrl = resetPasswordUrl;
    }
}
