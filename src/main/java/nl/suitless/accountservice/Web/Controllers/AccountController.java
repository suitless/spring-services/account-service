package nl.suitless.accountservice.Web.Controllers;

import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.ResetPassword;
import nl.suitless.accountservice.Services.interfaces.IAccountService;
import nl.suitless.accountservice.Services.interfaces.IResetPasswordService;
import nl.suitless.accountservice.Web.HateoasResources.AccountResource;
import nl.suitless.accountservice.Web.HateoasResources.AccountsResource;
import nl.suitless.accountservice.Web.Wrapper.CreateAccountRequestModel;
import nl.suitless.accountservice.Web.Wrapper.ForgotAccountRequestModel;
import nl.suitless.accountservice.Web.Wrapper.ResetAccountRequestModel;
import nl.suitless.accountservice.Web.Wrapper.SendTemplateEmailModel;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RequestMapping(path = "")
@RestController
public class AccountController {
    private IAccountService accountService;
    private IResetPasswordService resetPasswordService;

    private RestTemplate restTemplate;
    private EurekaClient eurekaClient;

    @Autowired
    public AccountController(IAccountService accountService, IResetPasswordService resetPasswordService, @Qualifier("eurekaClient") EurekaClient eurekaClient) {
        this.accountService = accountService;
        this.resetPasswordService = resetPasswordService;
        this.eurekaClient = eurekaClient;
        this.restTemplate = new RestTemplate();
    }

    @PostMapping(path = "/")
    public ResponseEntity<AccountResource> createAccount(@RequestBody CreateAccountRequestModel reqModel) {
        AccountResource resource = new AccountResource(accountService.createAccount(reqModel.getEmail(), reqModel.getPassword(), reqModel.getFirstName(), reqModel.getLastName()));
        resource.add(linkTo(methodOn(AccountController.class).createAccount(reqModel)).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @PostMapping(path = "/forgot")
    public ResponseEntity<Void> forgotPassword(@RequestBody ForgotAccountRequestModel reqModel, @RequestHeader(name = "Authorization") String token) {
        resetPasswordService.createResetPasswordToken(reqModel.getEmail(), reqModel.getResetPasswordUrl());

        Application emailService = eurekaClient.getApplication("email-service");
        InstanceInfo instanceInfo = emailService.getInstances().get(0);
        String url = "http://" + instanceInfo.getIPAddr() + ":" + instanceInfo.getPort() + "/send/template/single";

        SendTemplateEmailModel emailModel = new SendTemplateEmailModel(reqModel.getEmail(), "resetPasswordTemplate", new String[]{});
        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", token);
        HttpEntity<SendTemplateEmailModel> request = new HttpEntity<>(emailModel, headers);
        restTemplate.postForEntity(url, request, Void.class);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/reset")
    public ResponseEntity<Void> resetPassword(@RequestBody ResetAccountRequestModel reqModel){
        Account foundAccount = accountService.getAccountByEmail(reqModel.getEmail());
        resetPasswordService.resetPassword(foundAccount, reqModel.getResetToken(), reqModel.getNewPassword());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<AccountResource> getAccount(@PathVariable("id") String id) {
        AccountResource resource = new AccountResource(accountService.getAccount(id));
        resource.add(linkTo(methodOn(AccountController.class).getAccount(resource.getAccount().getAccountID())).withSelfRel());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @GetMapping(path = "/")
    public ResponseEntity<AccountsResource> getAllAccounts() {
        AccountsResource resource = new AccountsResource(accountService.getAllAccounts());

        return new ResponseEntity<>(resource, HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteAccount(@PathVariable("id") String id) {
        accountService.deleteAccount(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
