package nl.suitless.accountservice.Services;

import nl.suitless.accountservice.Data.IAccountRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.Role;
import nl.suitless.accountservice.Domain.Wrapper.CustomUserWrapper;
import nl.suitless.accountservice.Web.Security.JWTLoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * authentication logic required to check if a user exists within the system.
 * after this it will be checked internally by spring using {@link org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder} to decrypt and check if it matches.
 * The loaduserbyusername function will be called by {@link JWTLoginFilter#attemptAuthentication(HttpServletRequest, HttpServletResponse)}
 */
@Service
public class AuthService implements UserDetailsService {
    private IAccountRepository accountRepository;

    @Autowired
    public AuthService(IAccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    //We use email internally, in spring security context this will be handled as any other username
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByEmail(username.toLowerCase()).orElseThrow(() -> new UsernameNotFoundException(username));
        return new CustomUserWrapper(account.getAccountID(), account.getEmail(), account.getPassword(), getUserAuthority(account.getRoles()));
    }

    private List<GrantedAuthority> getUserAuthority(final Collection<Role> userRoles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for(Role role : userRoles) {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return authorities;
    }
}
