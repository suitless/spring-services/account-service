package nl.suitless.accountservice;

import nl.suitless.accountservice.Data.IAccountRepository;
import nl.suitless.accountservice.Data.IRoleRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.Role;
import nl.suitless.accountservice.Exceptions.AccountAlreadyExistsException;
import nl.suitless.accountservice.Exceptions.AccountNotFoundException;
import nl.suitless.accountservice.Services.AccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceUnitTests {
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	private Account account;

	@Mock
	private IAccountRepository accountRepository;
	@Mock
	private IRoleRepository roleRepository;

	@InjectMocks
	private AccountService accountService;

	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		account = new Account("mail@mail.nl", "password", "Jan"," Piet");
		account.setAccountID("5b992f05-8443-4c2e-b9e8-3ca523c4525c");
	}

	@Test
	public void findUserByIDExists() {
		when(accountRepository.findById("5b992f05-8443-4c2e-b9e8-3ca523c4525c")).thenReturn(Optional.of(account));

		Account foundAccount = accountService.getAccount("5b992f05-8443-4c2e-b9e8-3ca523c4525c");

		verify(accountRepository, times(1)).findById("5b992f05-8443-4c2e-b9e8-3ca523c4525c");
		Assert.assertEquals(foundAccount, account);
	}

	@Test
	public void findUserByIDDoesNotExist() {
		when(accountRepository.findById(anyString())).thenReturn(Optional.empty());

		exception.expect(AccountNotFoundException.class);

		accountService.getAccount("5b992f05-8443-4c2e-b9e8-3ca523c4525c");
	}

	@Test
	public void createUserAlreadyExists() {
		when(accountRepository.findByEmail("mail@mail.nl")).thenReturn(Optional.of(account));

		exception.expect(AccountAlreadyExistsException.class);
		accountService.createAccount("mail@mail.nl", "password", "jan", "piet");
	}

	@Test
	public void createUserValid() {
		Account newAccount = new Account("mail@mail.nl", "password", "jan", "piet");

		when(accountRepository.findByEmail("mail@mail.nl")).thenReturn(Optional.empty());
		when(roleRepository.findByName("USER")).thenReturn(Optional.of(new Role("USER")));
		when(accountRepository.save(any(Account.class))).thenReturn(newAccount);

		Account returnedAccount = accountService.createAccount("mail@mail.nl", "password", "jan", "piet");

		verify(accountRepository, times(1)).save(newAccount);
		Assert.assertEquals(newAccount.getEmail(), returnedAccount.getEmail());
	}

	@Test
	public void deleteUserExists() {
		when(accountRepository.findById(account.getAccountID())).thenReturn(Optional.of(account));

		accountService.deleteAccount(account.getAccountID());

		verify(accountRepository, times(1)).delete(account);
	}

	@Test
	public void deleteUserDoesNotExist() {
		when(accountRepository.findById(account.getAccountID())).thenReturn(Optional.empty());

		exception.expect(AccountNotFoundException.class);

		accountService.deleteAccount(account.getAccountID());
	}

}

