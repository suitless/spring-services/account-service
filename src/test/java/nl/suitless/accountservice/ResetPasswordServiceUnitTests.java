package nl.suitless.accountservice;

import nl.suitless.accountservice.Data.IResetPasswordRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.ResetPassword;
import nl.suitless.accountservice.Exceptions.ResetTokenNotMatchedException;
import nl.suitless.accountservice.Services.ResetPasswordService;
import nl.suitless.accountservice.Services.interfaces.IAccountService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResetPasswordServiceUnitTests {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private ResetPassword resetPassword;

    @Mock
    private IResetPasswordRepository repository;
    @Mock
    private IAccountService accountService;

    @InjectMocks
    private ResetPasswordService resetPasswordService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        resetPassword = new ResetPassword("mail@mail.com", "http://resetUrl/");
        resetPassword.setResetToken("e76b307d-158d-4365-bbcf-3d5cfa336d90");
    }

    @Test
    public void createResetPasswordTokenValid() {
        when(repository.findByEmail(anyString())).thenReturn(Optional.of(resetPassword));

        ResetPassword newResetPassword = resetPasswordService.createResetPasswordToken("mail@mail.com", "http://resetUrl/");

        verify(repository, times(0)).save(any(ResetPassword.class));
        Assert.assertEquals(resetPassword, newResetPassword);
    }

    @Test
    public void createResetPasswordTokenAlreadyExistsValid() {
        ResetPassword otherResetPassword = new ResetPassword("mail@mail.com", "http://resetUrl/");
        otherResetPassword.setResetToken("dc9cb5cf-9722-45bf-bc24-2fab0a100f16");

        when(repository.findByEmail(anyString())).thenReturn(Optional.empty());
        when(repository.save(any(ResetPassword.class))).thenReturn(otherResetPassword);

        ResetPassword newResetPassword = resetPasswordService.createResetPasswordToken("mail@mail.com", "http://resetUrl/");

        verify(repository, times(1)).save(any(ResetPassword.class));
        Assert.assertNotEquals(newResetPassword.getResetToken(), resetPassword.getResetToken());
    }

    @Test
    public void resetPasswordValid() {
        Account account = new Account("mail@mail.com", "securePassw", "Yeet", "Meister");

        when(repository.findByEmail(anyString())).thenReturn(Optional.of(resetPassword));

        resetPasswordService.resetPassword(account, resetPassword.getResetToken(), "newPassword");

        verify(repository, times(1)).delete(any(ResetPassword.class));
    }

    @Test
    public void resetPasswordNotFound() {
        Account account = new Account("mail@mail.com", "securePassw", "Yeet", "Meister");

        when(repository.findByEmail(anyString())).thenReturn(Optional.empty());

        exception.expect(ResetTokenNotMatchedException.class);

        resetPasswordService.resetPassword(account, resetPassword.getResetToken(), "newPassword");
    }

    @Test
    public void resetPasswordNotMatched() {
        Account account = new Account("mail@mail.com", "secirePasw", "Yeet", "Meister");
        ResetPassword otherResetPassword = new ResetPassword("mail@mail.com", "http://resetUrl/");
        otherResetPassword.setResetToken("dc9cb5cf-9722-45bf-bc24-2fab0a100f16");

        when(repository.findByEmail(anyString())).thenReturn(Optional.of(otherResetPassword));

        exception.expect(ResetTokenNotMatchedException.class);

        resetPasswordService.resetPassword(account, resetPassword.getResetToken(), "newPassword");
    }
}
