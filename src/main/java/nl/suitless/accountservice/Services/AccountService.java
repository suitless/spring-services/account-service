package nl.suitless.accountservice.Services;

import nl.suitless.accountservice.Data.IAccountRepository;
import nl.suitless.accountservice.Data.IRoleRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.Role;
import nl.suitless.accountservice.Exceptions.AccountAlreadyExistsException;
import nl.suitless.accountservice.Exceptions.AccountNotFoundException;
import nl.suitless.accountservice.Services.interfaces.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.List;

@Service
public class AccountService implements IAccountService {
    private IRoleRepository roleRepository;
    private IAccountRepository accountRepository;

    @Autowired
    public AccountService(IAccountRepository accountRepository, IRoleRepository roleRepository) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public Account createAccount(String email, String password, String firstName, String lastName) {
        //check if user with email already exists
        checkIfAccountAlreadyExists(email);

        //encrypt password and create account
        String encryptedPass = BCrypt.hashpw(password, BCrypt.gensalt());
        Account newAccount = new Account(email, encryptedPass, firstName, lastName);

        //give user default 'USER' role
        Role role = roleRepository.findByName("USER").orElse(new Role("USER"));
        newAccount.getRoles().add(role);
        role.getAccounts().add(newAccount);

        //save account to database and return it.
        return accountRepository.save(newAccount);
    }

    @Override
    public Account updateAccount(String id, String email, String password, String firstName, String lastName) {
        Account account = getAccountIfPresent(id);
        account.setEmail(email);
        account.setFirstName(firstName);
        account.setLastName(lastName);
        account.setPassword(password);
        return accountRepository.save(account);
    }

    @Override
    public Account getAccountByEmail(String email) {
        return accountRepository.findByEmail(email).orElseThrow(() -> new AccountNotFoundException("Account with email: " + email + " Not found"));
    }

    @Override
    public Account getAccount(String id) {
        return getAccountIfPresent(id);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.getAllByFirstNameIsNotNull().orElseThrow(() ->
                new AccountNotFoundException("No accounts found"));
    }

    @Override
    public void deleteAccount(String id) {
        Account account = getAccountIfPresent(id);
        accountRepository.delete(account);
    }

    //region Generic methods
    private Account getAccountIfPresent(String id) {
        Account account = accountRepository.findById(id).orElseThrow(() ->  new AccountNotFoundException("Account with id : " + id + " Not found"));
        account.clearPassword();
        return account;
    }

    private void checkIfAccountAlreadyExists(String email) {
        Optional<Account> foundAccount = accountRepository.findByEmail(email);
        if(foundAccount.isPresent()) {
            throw new AccountAlreadyExistsException("Account with email : " + email + " already exists");
        }
    }
    //endregion
}
