package nl.suitless.accountservice;

import nl.suitless.accountservice.Data.IAccountRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.Role;
import nl.suitless.accountservice.Services.AuthService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceUnitTests {
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Mock
    private IAccountRepository accountRepository;

    @InjectMocks
    private AuthService authService;

    private Account account;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        account = new Account("mail@mail.nl", "password", "Jan"," Piet");
        account.getRoles().add(new Role("USER"));
        accountRepository.save(account);
    }

    @Test
    public void findUserByUsernameExists() {
        when(accountRepository.findByEmail(anyString())).thenReturn(Optional.of(account));

        UserDetails foundAccount = authService.loadUserByUsername(account.getEmail());

        verify(accountRepository, times(1)).findByEmail(account.getEmail());
        Assert.assertEquals(foundAccount.getUsername(), account.getEmail());
        Assert.assertEquals(foundAccount.getAuthorities().size(), 1);
    }

    @Test
    public void findUserByUsernameDoesNotExist() {
        when(accountRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        exception.expect(UsernameNotFoundException.class);

        UserDetails foundAccount = authService.loadUserByUsername(account.getEmail());
    }

}
