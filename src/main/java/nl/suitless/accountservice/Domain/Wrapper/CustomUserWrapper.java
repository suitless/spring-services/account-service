package nl.suitless.accountservice.Domain.Wrapper;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class CustomUserWrapper extends User {
    private String userID;

    /** Spring provided class {@link User} requires the use of username, since we do not work with usernames we will store the users email inside this instead.
     * ~ Nick
     */
    public CustomUserWrapper(String userID, String username, String password,
                             Collection<? extends GrantedAuthority> authorities) {
        super(username,password, authorities);
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }
}
