package nl.suitless.accountservice.Services;

import nl.suitless.accountservice.Data.IResetPasswordRepository;
import nl.suitless.accountservice.Domain.Account;
import nl.suitless.accountservice.Domain.ResetPassword;
import nl.suitless.accountservice.Exceptions.ResetTokenNotMatchedException;
import nl.suitless.accountservice.Services.interfaces.IAccountService;
import nl.suitless.accountservice.Services.interfaces.IResetPasswordService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResetPasswordService implements IResetPasswordService {
    private IResetPasswordRepository repository;
    private IAccountService accountService;

    @Autowired
    public ResetPasswordService(IResetPasswordRepository repository, IAccountService accountService) {
        this.repository = repository;
        this.accountService = accountService;
    }

    @Override
    public ResetPassword createResetPasswordToken(String email, String resetPasswordUrl) {
        Optional<ResetPassword> foundResetPassword = repository.findByEmail(email);

        return foundResetPassword.orElseGet(() -> repository.save(new ResetPassword(email, resetPasswordUrl)));
    }

    @Override
    public void resetPassword(Account account, String resetToken, String newPassword) {
        ResetPassword resetPassword = repository.findByEmail(account.getEmail()).orElseThrow(() -> new ResetTokenNotMatchedException("Couldn't reset your password"));
        if(!resetPassword.getResetToken().equals(resetToken)){
            throw new ResetTokenNotMatchedException("Couldn't reset your password");
        }

        String encryptedPass = BCrypt.hashpw(newPassword, BCrypt.gensalt());
        account.setPassword(encryptedPass);
        accountService.updateAccount(account.getAccountID(), account.getEmail(), account.getPassword(), account.getFirstName(), account.getLastName());
        repository.delete(resetPassword);
    }
}
