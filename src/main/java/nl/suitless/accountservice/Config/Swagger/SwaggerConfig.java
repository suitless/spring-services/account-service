package nl.suitless.accountservice.Config.Swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("nl.suitless.accountservice.Web.Controllers"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Account Service",
                "The account service is a server that stores all accounts and act as a authentication server",
                "Latest release",
                "Terms of service",
                new Contact("Suitless", "http://suitless.nl/", "ehvLINC@gmail.com"),
                "GPLv3 License", "https://gitlab.com/suitless/account-service/blob/master/LICENSE", Collections.emptyList());
    }
}
