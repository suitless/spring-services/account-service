FROM openjdk
COPY /build/libs/com.ehvlinc.account-service-0.0.1-SNAPSHOT.jar run.jar
EXPOSE 8090
CMD "java" "-jar" "run.jar"
